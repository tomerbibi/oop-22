﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace oop___22
{
    public class Camp
    {
        // i changed all for the challenge
        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int NumberOfPeople { get; set; }
        public int NumberOfTants { get; set; }
        public int NumberOfFleshLights { get; set; }
        private static int _lastCampId = 0;

        public Camp(double latitude, double longitude, int numberOfPeople, int numberOfTants, int numberOfFleshLights)
        {
            _lastCampId++;
            Id = _lastCampId;
            Latitude = latitude;
            Longitude = longitude;
            NumberOfPeople = numberOfPeople;
            NumberOfTants = numberOfTants;
            NumberOfFleshLights = numberOfFleshLights;
        }

        public Camp()
        {
        }

        public static bool operator == (Camp c1, Camp c2)
        {
            if (c1 is null && c2 is null)
                return true;
            if (c1 is null || c2 is null)
                return false;

            return c1.Id == c2.Id;
        }
        public static bool operator != (Camp c1, Camp c2)
        {
            return !(c1 == c2);
        }
        public static bool operator > (Camp c1, Camp c2)
        {
            if (c1 is null || c2 is null)
                return false;

            return c1.NumberOfPeople > c2.NumberOfPeople;
        }
        public static bool operator <(Camp c1, Camp c2)
        {
            return !(c1 > c2);
        }
        public override bool Equals(object obj)
        {
            return this == obj as Camp;
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public static Camp operator + (Camp c1, Camp c2)
        {
            Camp newCamp = new Camp(c1.Latitude + c2.Latitude, c1.Longitude + c2.Longitude, c1.NumberOfPeople + c2.NumberOfPeople,
                c1.NumberOfTants + c2.NumberOfTants, c1.NumberOfFleshLights + c2.NumberOfFleshLights);
            return newCamp;
        }
        public override string ToString()
        {
            return $"Id: {Id}, latitude: {Latitude}, Longitude: {Longitude}, NumberOfPeople: {NumberOfPeople}," +
                $" NumberOfTants: {NumberOfTants}, NumberOfFleshLights: {NumberOfFleshLights}";


        }
    }
}
