﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace oop___22
{
    class Program
    {
        static void Main(string[] args)
        {
            Camp c1 = new Camp(80, 90, 4, 1, 6);
            Camp c2 = new Camp(120, 98.5, 8, 2, 10);
            if (c2 > c1)
                Console.WriteLine("c2 is bigger");
            Camp c3 = c1 + c2;
            Console.WriteLine(c3.ToString());

            using (Stream file = new FileStream (@"..\..\camp.xml", FileMode.Create))
            {
                XmlSerializer serelizer = new XmlSerializer(typeof(Camp));
                serelizer.Serialize(file, c3);
            }
            Camp camp1;
            Camp camp2;
            using (Stream file = new FileStream(@"..\..\camp.xml", FileMode.Open))
            {
                XmlSerializer serelizer = new XmlSerializer(typeof(Camp));
                camp1 = serelizer.Deserialize(file) as Camp;
            }
            using (Stream file = new FileStream(@"..\..\camp.xml", FileMode.Open))
            {
                XmlSerializer serelizer = new XmlSerializer(typeof(Camp));
                camp2 = serelizer.Deserialize(file) as Camp;
            }
            Console.WriteLine($"are they the same camp? {camp1 == camp2}");
            Console.WriteLine($"does they have the same HashCode? {camp1.GetHashCode() == camp2.GetHashCode()}");

        }
    }
}
